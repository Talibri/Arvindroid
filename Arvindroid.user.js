// ==UserScript==
// @name         Arvindroid
// @namespace    http://tampermonkey.net/
// @version      1.2.2
// @description  Auto-battle with incredible skill!
// @author       Gnomez
// @match        *://*.talibri.com/*
// @grant        none
// ==/UserScript==

(function() {
  'use strict';

  let timingLogger = {
    start: 0,
    last: 0,
    task: '',
    begin: function(task) {
      this.task = task;
      this.start = Date.now();
      this.last = Date.now();
      console.log(`${this.task}: Start`);
    },
    check: function(step = '') {
      let now = Date.now();
      console.log(`${this.task}.${step}: ${now - this.last}`);
      this.last = now;
    },
    end: function() {
      this.check('end');
      console.log(`${this.task}: Total runtime ${this.last - this.start}`);
    }
  };
  
  let taskScheduler = {
    task: null,
    delay: 1000,
    
    timeout: null,
    args: [],
    
    getBoundTrigger: function() {
      let myBoundTrigger = this.trigger.bind(this);
      this.getBoundTrigger = function() {
        return myBoundTrigger;
      };
      return myBoundTrigger;
    },
    trigger: function() {
      this.task.apply(null, this.args);
      this.clear();
    },
    set: function(...args) {
      clearTimeout(this.timeout);
      this.args = args;
      this.timeout = setTimeout(this.getBoundTrigger(), this.delay);
    },
    clear: function() {
      clearTimeout(this.timeout);
      this.timeout = null;
      this.args = [];
    },
    resolve: function() {
      clearTimeout(this.timeout);
      if (this.timeout !== null) {
        this.trigger();
      }
    },
    
    create: function(task) {
      let res = Object.create(taskScheduler);
      res.task = task;
      return res;
    }
  };
  
  
  
  /*
   * Define Arvindroid datamodel
   */
   
  const ACTIONS = {
    'Item': {
      placeholder: '1-5',
      showArg: true
    },
    'Skill': {
      placeholder: '1-5',
      showArg: true
    },
    'Flee': {
      placeholder: 'Buh-bye!',
      showArg: false
    },
    'Zone': {
      placeholder: '9',
      showArg: true
    },
    'AND': {
      placeholder: 'One more thing...',
      showArg: false
    },
    'OR': {
      placeholder: 'I\'m going to let you up now...',
      showArg: false
    },
    '{': {
      placeholder: 'Hi, there!',
      showArg: false
    },
    'Combo': {
      placeholder: 'Blablabla Tidal Wave!',
      showArg: false
    },
    'Load': {
      placeholder: 'Set Name',
      showArg: true
    }
  };
              
  const CONDITIONS = {
    'If Possible': {
      test: agree,
      placeholder: 'Never not acting!',
      showArg: false,
      showAction: true
    },
    'Probability': {
      test: randomBelow,
      placeholder: '0-100',
      showArg: true,
      showAction: true
    },
    'Player HP Below': {
      test: playerHpBelow,
      placeholder: '42',
      showArg: true,
      showAction: true
    },
    'Player HP Missing': {
      test: playerHpMissing,
      placeholder: '24',
      showArg: true,
      showAction: true
    },
    'Player MP Below': {
      test: playerMpBelow,
      placeholder: '27',
      showArg: true,
      showAction: true
    },
    'Player MP Missing': {
      test: playerMpMissing,
      placeholder: '72',
      showArg: true,
      showAction: true
    },
    'Enemy Name': {
      test: enemyIsNamed,
      placeholder: 'Scary Dragon',
      showArg: true,
      showAction: true
    },
    'Enemy HP Below': {
      test: enemyHpBelow,
      placeholder: '69',
      showArg: true,
      showAction: true
    },
    'Enemy HP Above': {
      test: enemyHpAbove,
      placeholder: '96',
      showArg: true,
      showAction: true
    },
    'Status Active': {
      test: statusActive,
      placeholder: 'icon alt',
      showArg: true,
      showAction: true
    },
    'Status Inactive': {
      test: statusInactive,
      placeholder: 'icon alt',
      showArg: true,
      showAction: true
    },
    'Current Zone': {
      test: combatZoneIs,
      placeholder: 'zone ID',
      showArg: true,
      showAction: true
    },
    '}': {
      test: disagree,
      placeholder: 'Now, where was I...',
      showArg: false,
      showAction: false
    },
    'COMBO TIME': {
      test: disagree,
      placeholder: '...sort of.',
      showArg: false,
      showAction: true
    }
  };
  
  function disagree() { return false; }
  function agree() { return true; }
  
  function playerHpBelow(threshold) {
    threshold = parseInt(threshold);
    return isNaN(threshold) ? false : combat.state.currentHp < threshold;
  }
  function playerHpMissing(threshold) {
    threshold = parseInt(threshold);
    return isNaN(threshold) ? false : combat.state.maxHp - combat.state.currentHp >= threshold;
  }
  function playerMpBelow(threshold) {
    threshold = parseInt(threshold);
    return isNaN(threshold) ? false : combat.state.currentMp < threshold;
  }
  function playerMpMissing(threshold) {
    threshold = parseInt(threshold);
    return isNaN(threshold) ? false : combat.state.maxMp - combat.state.currentMp >= threshold;
  }

  function enemyIsNamed(name) {
    return name === combat.state.enemyName;
  }
  function enemyHpBelow(threshold) {
    threshold = parseInt(threshold);
    return isNaN(threshold) ? false : combat.state.enemyHp < threshold;
  }
  function enemyHpAbove(threshold) {
    threshold = parseInt(threshold);
    return isNaN(threshold) ? false : combat.state.enemyHp > threshold;
  }
  
  function statusActive(alt) {
    return combat.state.statusEffects[alt];
  }
  function statusInactive(alt) {
    return !combat.state.statusEffects[alt];
  }
  
  function randomBelow(threshold) {
    threshold = parseInt(threshold);
    var percentile = Math.floor(Math.random() * 100);
    return isNaN(threshold) ? false : percentile < threshold;
  }
  
  function combatZoneIs(id) {
    return id == status.settings.combatZone;
  }

  
  /*
   * Define CSS
   */
  
  const CSS_COLORS = {
    grey: {
      light: '#888',
      dark: '#888'
    },
    blue: {
      light: '#5bc0de',
      dark: '#3e3eff'
    },
    green: {
      light: '#5cb85c',
      dark: '#0a0'
    },
    yellow: {
      light: '#f0ad4e',
      dark: '#da0'
    },
    red: {
      light: '#d9534f',
      dark: '#d00'
    },
    
    mainBg: {
      light: '#fff',
      dark: '#333'
    },
    stripeBg: {
      light: '#f0f0f0',
      dark: '#3a3a3a'
    },
    hoverBg: {
      light: '#c0c0ff',
      dark: '#5f5f33'
    },
    statusBg: {
      light: '#efefbb',
      dark: '#3a3a53'
    },
    
    mainText: {
      light: '#333',
      dark: '#ccc'
    },
    buttonText: {
      light: '#fff',
      dark: '#fff'
    }
  };
    
  let css = document.createElement('style');
  let makeColorBlock = function(subselectors, ...pairs) {
    let blocks = [];
    Object.keys(CSS_COLORS.grey).forEach(function(theme) {
      blocks.push(`
    ${subselectors.map(function(subselector) { return `${'#'}arvindroid[theme=${theme}] ${subselector}`; }).join(',\n')} {
      ${pairs.map(function(pair) { return `${pair[0]}: ${CSS_COLORS[pair[1]][theme]};`; }).join('\n')}
    }`
      );
    });
    return blocks.join('\n');
  };
  
  css.innerHTML = `
    #arvindroid ul.dropdown-menu {
      overflow: hidden;
      max-height: none;
      white-space: nowrap;
    }
    ${makeColorBlock(['ul.dropdown-menu'],
      ['background-color', 'statusBg'], 
      ['color', 'mainText']
    )}
    
    #arvindroid .dropdown-menu {
      display: block;
      visibility: hidden;
    }
    #arvindroid.open > .dropdown-menu {
      visibility: visible;
    }
    
    
    #arvindroid .avd-plist {
      // overflow: hidden;
      // max-height: 480px;
      position: relative;
    }
    ${makeColorBlock(['.avd-plist'],
      ['background-color', 'mainBg']
    )}
    
    .avd-plist__scroll {
      overflow-y: scroll;
      height: 240px;
    }
    .avd-plist__scroll > div {
      width: 520px;
    }
    
    .avd-plist__viewport {
      position: absolute;
      top: 0;
      left: 0;
      overflow: hidden;
      width: 520px;
      height: 240px;
    }
    .avd-plist__display {
      position: absolute;
      top: 0;
      left: 0;
      overflow-y: scroll;
      height: 240px;
    }
    .avd-pline {
      width: 520px;
    }
    .avd-plist__filler {
      height: 240px;
    }
    
    
    #arvindroid input {
      height: 20px;
      padding: 0px;
      width: 100px;
    }

    #arvindroid select {
      height: 20px;
      padding: 0px;
    }
    
    #arvindroid .avd-cline__btn {
      padding: 0px 5px;
      margin: 0px 5px;
      
      border: 1px solid transparent;
      border-radius: 4px;
    }
    ${makeColorBlock(['.avd-cline__btn'],
      ['color', 'buttonText']
    )}
    #arvindroid .avd-cline__btn:hover {
      filter: brightness(120%);
    }
    #arvindroid .avd-cline__btn:active {
      filter: brightness(80%);
    }
    
    ${makeColorBlock(['.avd-cline__btn--info'], 
      ['background-color', 'blue']
    )}
    ${makeColorBlock(['.avd-cline__btn--success'], 
      ['background-color', 'green']
    )}
    ${makeColorBlock(['.avd-cline__btn--warning'], 
      ['background-color', 'yellow']
    )}
    ${makeColorBlock(['.avd-cline__btn--danger'], 
      ['background-color', 'red']
    )}
    
    #arvindroid input.avd-hidden,
    #arvindroid select.avd-hidden {
      visibility: hidden;
    }
    
    ${makeColorBlock(['input', 'select'],
      ['background-color', 'mainBg']
    )}
    
    
    .avd-cline,
    .avd-pline {
      padding: 2px 0px;
      
      display: flex;
      flex-flow: row nowrap;
      justify-content: space-evenly;
      align-items: center;
    }
    #arvindroid .flex-fill {
      flex-grow: 1;
    }
    
    
    #arvindroid .avd-cline {
      margin: 5px 0px;
      height: 26px;
      text-align: left;
    }
    
    #arvindroid .avd-cline > :first-child {
      margin-left: 45px;
    }
    
    #arvindroid .avd-cline > :last-child {
      margin-right: 45px;
    }
    
    #arvindroid .avd-cline input,
    #arvindroid .avd-cline select {
      margin: 0 5px;
    }
    
    
    ${makeColorBlock(['li.submenu'],
      ['background-color', 'hoverBg']
    )}
    
    
    ${makeColorBlock(['.avd-pline:nth-child(even)'],
      ['background-color', 'mainBg']
    )}
    ${makeColorBlock(['.avd-pline:nth-child(odd)'],
      ['background-color', 'stripeBg']
    )}
    ${makeColorBlock(['.avd-pline:hover'],
      ['background-color', 'hoverBg']
    )}
    
    #arvindroid .avd-pline--hidden {
      visibility: hidden;
    }

    
    #arvindroid .avd-plist__highlight {
      display: inline-block;
      width: 10px;
      height: 17px;
      margin: 1px 5px;
      vertical-align: top;
    }
    ${makeColorBlock(['.avd-plist__highlight[highlight=grey]'],
      ['background-color', 'grey']
    )}
    ${makeColorBlock(['.avd-plist__highlight[highlight=blue]'],
      ['background-color', 'blue']
    )}
    ${makeColorBlock(['.avd-plist__highlight[highlight=green]'],
      ['background-color', 'green']
    )}
    ${makeColorBlock(['.avd-plist__highlight[highlight=yellow]'],
      ['background-color', 'yellow']
    )}
    ${makeColorBlock(['.avd-plist__highlight[highlight=red]'],
      ['background-color', 'red']
    )}
    #arvindroid.dim .avd-plist__highlight {
      opacity: 0.6;
    }
    
    
    #arvindroid span.badge {
      margin: 0px 5px;
      display: inline-block;
      height: 20px;
      padding: 4px 5px;
      cursor: pointer;
    }
    
    #arvindroid #arvindroid-theme-change {
      position: absolute;
      top: 2px;
      left: 2px;
      font-size: large;
      height: unset;
      z-index: 2;
    }
    
    
    #arvindroid #arvindroid-what-do {
      position: absolute;
      top: 2px;
      right: 2px;
      font-size: large;
      height: unset;
      z-index: 2;
    }
    
    .avd-help {
      display: flex;
      flex-flow: column nowrap;
      z-index: 1;
      background-color: gray;
      position: absolute;
      left: 0;
      top: 0;
      width: 100%;
      height: 100%;
      visibility: hidden;
    }
    .dropdown.open .avd-help--open {
      visibility: visible;
    }
    
    .avd-help__header {
      flex-shrink: 0;
      padding: 2px 0;
      border-bottom: 2px solid;
      text-align: center;
      font-size: large;
    }
    ${makeColorBlock(['.avd-help__header'],
      ['background-color', 'statusBg'],
      ['border-bottom-color', 'hoverBg']
    )}

    .avd-help__body {
      flex-grow: 1;
      min-height: 0;
      display: flex;
      flex-flow: row nowrap;
    }
    
    .avd-help__pages {
      list-style: none;
      padding: 5px 0 5px 5px;
      border-right: 2px solid;
      white-space: nowrap;
    }
    ${makeColorBlock(['.avd-help__pages'],
      ['background-color', 'stripeBg'],
      ['border-right-color', 'hoverBg']
    )}
    
    .avd-help__page {
      padding: 2px 10px 2px 5px;
      cursor: pointer;
    }
    ${makeColorBlock(['.avd-help__page--active'],
      ['background-color', 'hoverBg']
    )}

    .avd-help__content {
      flex-grow: 1;
      padding: 5px;
      white-space: normal;
      overflow-y: auto;
    }
    ${makeColorBlock(['.avd-help__content'],
      ['background-color', 'mainBg']
    )}
  `;
  document.head.appendChild(css);
  
  let FILLER_SPAN_HTML = '<span class="flex-fill"></span>';
  
  
  /*
   * Do The Thing
   */
   
   
  var dropdown = document.createElement('li'); // outermost navbar element
  dropdown.className = 'dropdown';
  dropdown.id = 'arvindroid';
  
  var droidUl = document.createElement('ul'); // dropdown contents
  droidUl.className = 'dropdown-menu';
  droidUl.addEventListener('click', function(e) { e.stopPropagation(); }); // prevent closing dropdown on click
  dropdown.appendChild(droidUl);
  
  
  const CROSS_MARK = '\u2716';
  const CHECK_MARK = '\u2714';
  const SPARKLES_MARK = '\u2728';
  const SQUARE_MARK = '\u25A0';
  const STAR_MARK = '\u2605';
  const HALF_CIRCLE_MARK = '\u25D1';
  
  function makeDefaultActionUsability() {
    return {'Item':  {'1': true,
                      '2': true,
                      '3': true,
                      '4': true,
                      '5': true
                     },
            'Skill': {'1': true,
                      '2': true,
                      '3': true,
                      '4': true,
                      '5': true
                     }
           };
  }
  
  let status = {
    settings: {
      version: '1.0',
      active: false,
      autoRestart: false,
      combatZone: -1,
      actionUsability: makeDefaultActionUsability(),
      primarySet: '',
      currentSet: '',
      theme: 'dark'
    },
    
    STORAGE_KEY: 'arvindroid-status',
    
    load: function() {
      let status = this;
      let storedStatus = localStorage.getItem(status.STORAGE_KEY);
      if (storedStatus !== null) {
        storedStatus = status.upgradeModel(JSON.parse(storedStatus));
        Object.keys(storedStatus).forEach(function(key) {
          if (status.settings[key] !== undefined) {
            status.settings[key] = storedStatus[key];
          }
        });
        ui.dirt.status = true;
      }
    },
    
    upgradeModel: function(oldStatus) {
      let upgradeFrom = {
        
        'undefined': function(oldStatus) {
          let newStatus = {};
          Object.keys(oldStatus).forEach(function(key) {
            if (key === 'skillUsability' || (key === 'actionUsability' && Array.isArray(oldStatus[key]))) {
              let skillUsability = oldStatus[key];
              newStatus.actionUsability = {};
              (['Item', 'Skill']).forEach(function(bar, barIndex) {
                newStatus.actionUsability[bar] = {};
                (['1', '2', '3', '4', '5']).forEach(function(action, actionIndex) {
                  newStatus.actionUsability[bar][action] = skillUsability[barIndex][actionIndex];
                });
              });
            }
            else {
              newStatus[key] = oldStatus[key];
            }
          });
          newStatus.version = '1.0';
          return upgradeFrom['1.0'](newStatus);
        },
        
        '1.0': function(oldStatus) {
          return oldStatus;
        }
        
      };
      return upgradeFrom[oldStatus.version](oldStatus);
    },
    
    store: function() {
      localStorage.setItem(this.STORAGE_KEY, JSON.stringify(this.settings));
    },
    
    storageScheduler: taskScheduler.create( function() { status.store(); } ),
    
    setActionUsability: function(bar, action, state) {
      if (this.settings.actionUsability.hasOwnProperty(bar)) {
        if (this.settings.actionUsability[bar].hasOwnProperty(action)) {
          this.settings.actionUsability[bar][action] = state;
          this.storageScheduler.set();
          ui.dirt.status = true;
        }
      }
    },
    
    getActionUsability: function(bar, action) {
      return this.settings.actionUsability[bar] === undefined ? undefined : this.settings.actionUsability[bar][action];
    },
    
    resetActionUsability: function() {
      this.settings.actionUsability = makeDefaultActionUsability();
      this.storageScheduler.set();
      ui.dirt.status = true;
    }
  };
  
  let appendSuffix = function(key, suffix) {
    return suffix === '' ? key : key + '-' + suffix;
  };
  
  let priorities = {
    model: {
      version: '1.0',
      data: [/*
        {
          condition: {
            type: "key into CONDITIONS",
            arg: "Some sort of parameter string"
          },
          action: {
            type: "key into ACTIONS",
            arg: "some sort of paramter string"
          }
        },
        ...
      */]
    },
    
    STORAGE_KEY: 'arvindroid-priorities',
    
    new: function() {
      return {
        condition: {type:'If Possible', arg:''},
        action: {type:'Item', arg:''}
      };
    },
    
    add: function(index = -1) {
      if (index === -1) {
        this.model.data.push(this.new());
      }
      else {
        this.model.data.splice(index, 0, this.new());
      }
      this.storageScheduler.set(status.settings.currentSet);
      ui.clearHighlights();
      ui.dirt.priorities = true;
    },
    
    remove: function(index = -1) {
      if (index === -1) {
        this.model.data.pop();
      }
      else {
        this.model.data.splice(index, 1);
      }
      this.storageScheduler.set(status.settings.currentSet);
      ui.clearHighlights();
      ui.dirt.priorities = true;
    },
    
    clear: function() {
      this.model.data = [];
      this.storageScheduler.set(status.settings.currentSet);
      ui.clearHighlights();
      ui.dirt.priorities = true;
    },
    
    load: function(keySuffix) {
      this.storageScheduler.resolve();
      let storedPriorities = localStorage.getItem(appendSuffix(this.STORAGE_KEY, keySuffix));
      if (storedPriorities !== null) {
        this.model = this.upgradeModel(JSON.parse(storedPriorities));
        ui.clearHighlights();
        ui.dirt.priorities = true;
      }
    },
    
    upgradeModel: function(oldModel) {
      let upgradeFrom = {
        
        'undefined': function(oldModel) {
          let newModel = {
            version: '1.0',
            data: []
          };
          const OLD_ACTIONS = [
            "Item",
            "Skill",
            "{",
            "Flee"
          ];
          const OLD_CONDITIONS = [
            "If Possible",
            "Player HP Below",
            "Player HP Missing",
            "Player MP Below",
            "Player MP Missing",
            "Enemy Name",
            "Enemy HP Below",
            "}"
          ];
          for (let i = 0; i < oldModel.length; i++) {
            let newPriority = priorities.new();
            let oldPriority = oldModel[i];
            if (typeof(oldPriority.action.arg) === typeof(0)) {
              newPriority.action.arg = `${oldPriority.action.arg + 1}`;
            }
            else {
              newPriority.action.arg = oldPriority.action.arg;
            }
            if (typeof(oldPriority.action.type) === typeof(0)) {
              newPriority.action.type = OLD_ACTIONS[oldPriority.action.type];
              if (!newPriority.action.type) {
                newPriority.action.type = "Item";
              }
            }
            else {
              newPriority.action.type = oldPriority.action.type;
            }
            newPriority.condition.arg = oldPriority.condition.arg;
            if (typeof(oldPriority.condition.type) === typeof(0)) {
              newPriority.condition.type = OLD_CONDITIONS[oldPriority.condition.type];
              if (!newPriority.condition.type) {
                newPriority.condition.type = "If Possible";
              }
            }
            else {
              newPriority.condition.type = oldPriority.condition.type;
            }
            newModel.data.push(newPriority);
          }
          return upgradeFrom['1.0'](newModel);
        },
        
        '1.0': function(oldModel) {
          return oldModel;
        }
        
      };
      
      let newModel = upgradeFrom[oldModel.version](oldModel);
      newModel.data.forEach(function(p) {
        if (!CONDITIONS[p.condition.type]) {
          p.condition.type = "If Possible";
        }
        if (!ACTIONS[p.action.type]) {
          p.action.type = "Item";
        }
      });
      return newModel;
    },
    
    serialise: function() {
      return JSON.stringify(this.model);
    },
    
    store: function(keySuffix) {
      localStorage.setItem(appendSuffix(this.STORAGE_KEY, keySuffix), this.serialise());
    },
    
    storageScheduler: taskScheduler.create(function(keySuffix) { priorities.store(keySuffix); }),
    
    switch: function(keySuffix) {
      status.settings.currentSet = keySuffix;
      status.storageScheduler.set();
      ui.dirt.status = true;
      this.load(keySuffix);
    },
    
    delete: function(keySuffix) {
      localStorage.removeItem(appendSuffix(this.STORAGE_KEY, keySuffix));
    }
  };
  
  let combat = {
    state: {
      currentHp: 0,
      maxHp: 0,
      
      currentMp: 0,
      maxMp: 0,
      
      enemyName: '',
      enemyHp: 0,
      
      statusEffects: {},
      
      lastCombatText: ''
    },
    
    meta: {
      actions: {'Item': [], 'Skill': [], 'Flee':null}, // arrays of action button elements
      lastCombatLog: null,
      
      comboTarget: -1,
      
      priorityAction: null,
      priorityActionButton: null,
      usedPriorityAction: false
    },
    
    updateState: function() {
      let hpText = document.querySelector('p#user-stat-health span.percentage-circle-contents')
                   .textContent
                   .trim()
                   .split("/");
      this.state.currentHp = parseInt(hpText[0]);
      this.state.maxHp = parseInt(hpText[1]);

      let mpText = document.querySelector('p#user-stat-mana span.percentage-circle-contents')
                   .textContent
                   .trim()
                   .split("/");
      this.state.currentMp = parseInt(mpText[0]);
      this.state.maxMp = parseInt(mpText[1]);

      this.state.enemyName = document.querySelector('span.in-combat-enemy-name').textContent;
      this.state.enemyHp = parseInt(document.querySelector('div.enemy-details span.stat-health-value').textContent);

      this.state.statusEffects = {};
      document.querySelectorAll('div.enemy-details div.user-status-effects img, div.enemy-details div.user-status-effects + div img').forEach(function(img) {
        combat.state.statusEffects[img.getAttribute('alt')] = true;
      });
      
      this.meta.actions['Item'] = [null, null, null, null, null]; // item buttons
      let actionNodes = document.querySelector('div.user-item-bar').children;
      for (let index = 0; index < actionNodes.length; index++) {
        this.meta.actions['Item'][index] = actionNodes[index];
      }
      
      this.meta.actions['Skill'] = [null, null, null, null, null]; // skill buttons
      actionNodes = document.querySelector('div.user-skill-bar').children;
      for (let index = 0; index < actionNodes.length; index++) {
        this.meta.actions['Skill'][index] = actionNodes[index];
      }
      
      this.meta.actions['Flee'] = $("div.enemy-details > button.avd-cline__btn--danger").first();
      
      this.state.lastCombatText = this.meta.lastCombatLog === null ? '' : this.meta.lastCombatLog.textContent;
      if (this.meta.priorityAction !== null &&
          this.meta.usedPriorityAction &&
          status.getActionUsability(this.meta.priorityAction.type, this.meta.priorityAction.arg))
      {
        if (/(You used [^\.!]*\. You have 0 remaining\.|You have 0 [^\.!]* remaining.|You do not have any [^\.!]* remaining!|You don't have enough [^\.!]* to use that ability!|You do not have the [^\.!]* necessary to use that ability!)/.test(this.state.lastCombatText)) {
          status.setActionUsability(this.meta.priorityAction.type, this.meta.priorityAction.arg, false);
        }
      }
      if (status.settings.primarySet !== '' &&
          status.settings.primarySet !== status.settings.currentSet &&
          /Your next enemy is a/.test(this.state.lastCombatText))
      {
        priorities.switch(status.settings.primarySet);
      }
    },
    
    decideNextAction: function() {
      var skipGroups = 0; // depth of priority groups being skipped
      var skipAnd = false; // was the previous action a failed AND?
      var metOr = false; // condition is met via OR
      var passOr = false; // ignore block of ANDs/ORs
      var visitedConfigs = []; // set of configurations loaded while resolving this action
      var resetOnComboBreaker = this.meta.comboTarget !== -1;
      
      this.meta.priorityAction = null;
      this.meta.priorityActionButton = null;
      this.meta.usedPriorityAction = false;
      
      ui.clearHighlights();
      dropdown.classList.remove('dim');
      
      // new stuff - beginnings of a design to split the action switch statement out into the action definitions themselves;
      //             the decisionState object would provide an interface for meta-actions to interact with other priorities
      
      // let decisionState = {
        // index: 0,
        // done: false,
        // visitedConfigs: [],
        // restartOnComboBreaker: false
      // };
      
      // if (this.meta.comboTarget !== -1) {
        // decisionState.index = this.meta.comboTarget;
        // decisionState.restartOnComboBreaker = true;
      // }
      
      // if (this.meta.comboTarget !== -1) {
        // if (this.meta.comboTarget > i) {
          // continue;
        // }
        // else if (this.meta.comboTarget === i && p.condition.type === 'COMBO TIME') {
          // conditionMet = true;
        // }
        // else {
          // this.meta.comboTarget = -1;
          // i = resetOnComboBreaker ? -1 : i - 1;
          // resetOnComboBreaker = false;
          // continue;
        // }
      // } 
        
      // while (!decisionState.done) {
        // let p = priorities.model.data[decisionState.index];
        // let conditionMet = CONDITIONS[p.condition.type].test(p.condition.arg);
        
        // ui.conditionHighlights[decisionState.index] =
          // conditionMet ? 'green' :
          // p.condition.type === 'COMBO TIME' || p.condition.type === '}' ? 'grey' :
          // 'red';
        
        // if (CONDITIONS[p.condition.type].showAction) {
          // ACTIONS[p.action.type].do(decisionState, conditionMet);
        // }
        // if ()
      // }
      
      // return;
      
      // function checkCondition(decisionState) {
        
      // }
      // /new stuff
      
      for (let i = 0; i < priorities.model.data.length; i++) {
        let p = priorities.model.data[i];
        let conditionMet = false;
        let skipped = false;
        
        if (this.meta.comboTarget !== -1) {
          if (this.meta.comboTarget > i) {
            continue;
          }
          else if (this.meta.comboTarget === i && p.condition.type === 'COMBO TIME') {
            conditionMet = true;
          }
          else {
            this.meta.comboTarget = -1;
            i = resetOnComboBreaker ? -1 : i - 1;
            resetOnComboBreaker = false;
            continue;
          }
        } 
        else if (skipAnd) {
          conditionMet = false;
          skipped = true;
        }
        else if (skipGroups > 0) {
          conditionMet = false;
          skipped = true;
          if (p.condition.type === '}') {
            skipGroups--;
          }
        }
        else if (metOr) {
          conditionMet = true;
        }
        else {
          conditionMet = CONDITIONS[p.condition.type].test(p.condition.arg);
        }
        
        skipAnd = false;
        passOr = metOr;
        metOr = false;
        
        if (skipped || passOr) {
          ui.conditionHighlights[i] = 'grey';
        }
        else if (!conditionMet && (p.condition.type === 'COMBO TIME' || p.condition.type === '}')) {
          ui.conditionHighlights[i] =  'grey';
        }
        else {
          ui.conditionHighlights[i] =  conditionMet ? 'green' : 'red';
        }
        
        // find action
        if (CONDITIONS[p.condition.type].showAction) {
          switch(p.action.type) {
            
            case 'Item':
            case 'Skill':
              if (conditionMet) {
                if (!status.getActionUsability(p.action.type, p.action.arg)) { // trying to select an unusable skill
                  ui.actionHighlights[i] =  'red';
                  break;
                }
                let action = this.meta.actions[p.action.type][parseInt(p.action.arg) - 1];
                if (action !== undefined && action !== null && action.classList.contains("btn-default") && !action.disabled) {
                  if (action.textContent.trim().endsWith('(0)')) { // trying to use a depleted item
                    status.setActionUsability(p.action.type, p.action.arg, false);
                    ui.actionHighlights[i] =  'red';
                    break;
                  }
                  this.meta.priorityAction = p.action;
                  this.meta.priorityActionButton = action;
                  if (this.meta.comboTarget === i) {
                    this.meta.comboTarget++;
                  }
                  ui.actionHighlights[i] =  'green';
                  return;
                }
                else {
                  ui.actionHighlights[i] = 'red';
                  break;
                }
              }
              else {
                ui.actionHighlights[i] =  'grey';
              }
              break;
              
            case 'Flee':
              if (conditionMet) {
                ui.actionHighlights[i] =  'green';
                this.meta.priorityAction = p.action;
                return;
              }
              else {
                ui.actionHighlights[i] =  'grey';
              }
              break;
              
            case 'AND':
              if (passOr) {
                metOr = true;
                ui.actionHighlights[i] = 'grey';
              }
              else if (skipped) {
                skipAnd = true;
                ui.actionHighlights[i] =  'grey';
              }
              else if (!conditionMet) {
                skipAnd = true;
                ui.actionHighlights[i] =  'yellow';
              }
              else {
                ui.actionHighlights[i] =  'blue';
              }
              break;
              
            case 'OR':
              if (passOr) {
                metOr = true;
                ui.actionHighlights[i] = 'grey';
              }
              else if (skipped) {
                ui.actionHighlights[i] =  'grey';
              }
              else if (!conditionMet) {
                ui.actionHighlights[i] =  'grey';
              }
              else {
                metOr = true;
                ui.actionHighlights[i] =  'blue';
              }
              break;
              
            case '{':
              if (skipped) {
                skipGroups++;
                ui.actionHighlights[i] =  'grey';
              }
              else if (!conditionMet) {
                skipGroups++;
                ui.actionHighlights[i] =  'yellow';
              }
              else {
                ui.actionHighlights[i] =  'blue';
              }
              break;
              
            case 'Combo':
              if (skipped) {
                ui.actionHighlights[i] =  'grey';
              }
              else if (conditionMet) {
                this.meta.comboTarget = i+1;
                ui.actionHighlights[i] =  'blue';
              }
              else {
                ui.actionHighlights[i] =  'yellow';
              }
              break;
              
            case 'Load':
              if (conditionMet) {
                if (visitedConfigs.indexOf(p.action.arg) === -1) {
                  i = -1;
                  this.meta.comboTarget = -1;
                  resetOnComboBreaker = false;
                  visitedConfigs.push(p.action.arg);
                  priorities.switch(p.action.arg);
                }
                else {
                  ui.actionHighlights[i] =  'red';
                }
              }
              else {
                ui.actionHighlights[i] =  'grey';
              }
              break;
              
            case 'Zone':
              if (conditionMet) {
                let newZone = parseInt(p.action.arg);
                if (!isNaN(newZone)) {
                  status.settings.combatZone = newZone;
                  status.storageScheduler.set();
                  this.meta.priorityAction = p.action;
                  ui.actionHighlights[i] =  'green';
                  return;
                }
                ui.actionHighlights[i] =  'red';
              }
              else {
                ui.actionHighlights[i] =  'grey';
              }
              break;
              
            default:
              break;
          }
        }
      }

      this.meta.comboTarget = -1;
      return;
    }
  };

  let ui = {
    dirt: {
      status: false,
      priorities: false
    },
    sync: { // references to functions that sync the UI to the model; defined in the UI initialization section
      status: undefined,
      priorities: undefined
    },
    
    conditionHighlights: [],
    actionHighlights: [],
    
    clean: function(key) {
      if (this.dirt[key]) {
        this.sync[key]();
      }
    },
    
    update: function() {
      Object.keys(this.dirt).forEach(this.clean.bind(this));
    },
    
    clearHighlights: function() {
      for (let i = 0; i < this.conditionHighlights.length; i++) {
        this.conditionHighlights[i] = [''];
        this.actionHighlights[i] = [''];
      }
      this.dirt.priorities = true;
    }
  };
  
    
  /*
   * Status UI injection
   */
  let conditionSelectString = '<select>';
  Object.keys(CONDITIONS).forEach(function(value, index) {
    conditionSelectString += `<option>${value}</option>`;
  });
  conditionSelectString += '</select>';
    
  let actionSelectString = '<select>';
  Object.keys(ACTIONS).forEach(function(value, index) {
    actionSelectString += `<option>${value}</option>`;
  });
  actionSelectString += '</select>';
    
    
   
  {
    let anchor = document.createElement('a');
    anchor.setAttribute('href', '#');
    anchor.className = 'dropdown-toggle';
    anchor.setAttribute('data-toggle', 'dropdown');
    anchor.setAttribute('role', 'button');
    anchor.setAttribute('aria-haspopup', 'true');
    anchor.setAttribute('aria-expanded', 'false');
    
    let anchorHTML = [];
    let actionDotString = `<span>${SQUARE_MARK}</span>`;
    anchor.innerHTML = `
      <span style='display: block; position: absolute; top: -3px; width: 100%; text-align: center; left: 0;'>
        ${actionDotString}${actionDotString}${actionDotString}${actionDotString}${actionDotString}
      </span>
      <span style='display: block; position: absolute; bottom: 0px; width: 100%; text-align: center; left: 0;'>
        ${actionDotString}${actionDotString}${actionDotString}${actionDotString}${actionDotString}
      </span>
      Arvindroid <span id='arvindroid-status' class='badge'>${CROSS_MARK}</span>
    `;
    
    let statusBadge = anchor.children[2];
    let actionDots = [anchor.children[0].children, anchor.children[1].children];
    
    dropdown.appendChild(anchor);
    
    let themeChangeBadge = document.createElement('span');
    themeChangeBadge.id = 'arvindroid-theme-change';
    themeChangeBadge.className = 'badge';
    themeChangeBadge.innerHTML = `${HALF_CIRCLE_MARK}`;
    themeChangeBadge.addEventListener('click', function() {
      status.settings.theme = status.settings.theme === 'dark' ? 'light' : 'dark';
      dropdown.setAttribute('theme', status.settings.theme);
      status.storageScheduler.set();
    });
    droidUl.appendChild(themeChangeBadge);
    
    
    let droidHelp = document.createElement('div');
    droidHelp.className = 'avd-help';
    droidHelp.innerHTML = `
      <div class='avd-help__header'>What do!?</div>
      <div class='avd-help__body'>
        <ul class='avd-help__pages'>
        </ul>
        <div class='avd-help__content'>
        <div>
      </div>
    `;
    
    let activePage;
    let addHelpPage = function(name, content) {
      let newPage = document.createElement('li');
      newPage.className = 'avd-help__page';
      newPage.textContent = name;
      newPage.addEventListener('click', function() {
        droidHelp.children[1].children[1].innerHTML = content;
        activePage && activePage.classList.remove('avd-help__page--active');
        this.classList.add('avd-help__page--active');
        activePage = this;
      });
      droidHelp.children[1].children[0].appendChild(newPage);
    };
    
    addHelpPage('Overview',`
      <p>
        Arvindroid is a customisable combat bot designed to help you idle combat more efficiently.
        Each round of combat, Arvindroid chooses an action to perform based on a list of priorities you create.
        Arvindroid can use skills and items, flee and restart combat, or even switch zones to heal up more easily.
        Arvindroid also includes features for saving and automatically loading different priority sets, allowing
        for complex setups that automatically switch strategies based on the current enemy or your combat status.
      </p>
    `);
    addHelpPage('Status Controls', `
      <p>
        <button class="avd-cline__btn avd-cline__btn--danger">Toggle Status</button> - Activate/Deactivate Arvindroid
        <br>
        <button class="avd-cline__btn avd-cline__btn--danger">Toggle Auto Restart</button> - Enable/Disable auto-restart on death
      </p>
      
      <br>
      <p>
        <button class="avd-cline__btn avd-cline__btn--info">Reset Action Usability</button> - Make all actions usable; see the Action Usability help page for more details
      </p>
      
      <br>
      <p>
        The menu link includes a badge showing Arvindroid's status at a glance:
        <br>
        <span class='badge'>${CROSS_MARK}</span> - Arvindroid is inactive
        <br>
        <span class='badge'>${CHECK_MARK}</span> - Arvindroid is active
        <br>
        <span class='badge'>${STAR_MARK}</span> - Arvindroid is active and will auto-restart on death
      </p>
      
      <br>
      <p>
        The menu link includes ten <span style='color: ${CSS_COLORS.green.light}'>${SQUARE_MARK}</span> showing skill usability; see the Action Usability help page for more details
      </p>
    `);
    addHelpPage('Priorities', `
      <p>
        Priorities are what you use to customise Arvindroid's decisions in combat. A priority is made of two parts:
        a condition used to determine when the priority applies, and an action to take when it does. Each round of combat,
        Arvindroid will check the conditions of each priority starting from the top of the list and moving down. When a
        condition is met, Arvindroid will check whether the corresponding action can be performed. Once a usable combat action
        has been found, Arvindroid will display some colored highlights in the priority list to help visualize its decision process,
        then stop evaluating conditions and wait until the end of the combat round to perform the action.
      </p>
      
      <br>
      <p>
        To get started, use the <button class="avd-cline__btn avd-cline__btn--success">${SPARKLES_MARK} New Priority</button>
        button to add your first priority. You can then edit the priority's condition and action using the inputs that appear
        in the priority list:
      </p>
      <p>
        <span class='badge'>${SPARKLES_MARK}</span> - Click to add a new priority above this one
      </p>
      <p>
        ${conditionSelectString}<input type="text"></input>
        <br>
        Select a type for the condition, then input an argument
      </p>
      <p>
        ${actionSelectString}<input type="text"></input>
        <br>
        Select a type for the action, then input an argument
      </p>
      <p>
        <span class='badge'>${CROSS_MARK}</span> - Click to remove this priority from the list
      </p>
    `);
    addHelpPage('P. Highlights', `
      <p>
        Each time Arvindroid chooses an action, it puts colored highlights to the left of each condition
        and to the right of each action in the priority list to visualize its decision process.
      </p>
      
      <br>
      <p>
        These highlights apply to both conditions and actions:
      </p>
      <p>
        <span class="avd-plist__highlight" highlight="green"></span> -
        The condition was met or the action was selected
      </p>
      <p>
        <span class="avd-plist__highlight" highlight="red"></span> -
        The condition was checked but failed or the action was checked but found unusable
      </p>
      <p>
        <span class="avd-plist__highlight" highlight="grey"></span> -
        The condition or action was ignored
      </p>
      
      <br>
      <p>
        These highlights only apply to certain actions:
      </p>
      <p>
        <span class="avd-plist__highlight" highlight="blue"></span> -
        Because the corresponding condition was met, some following conditions were checked
      </p>
      <p>
        <span class="avd-plist__highlight" highlight="yellow"></span> -
        Because the corresponding condition was not met, some following conditions were ignored
      </p>
    `);
    addHelpPage('Priority Sets', `
      <p>
        Arvindroid allows you to name, save and load multiple priority sets. This allows you to
        have different priority sets for training different affinities or fighting in different
        zones; you can even select a primary set to start each battle with and use the Load action
        to switch sets on the fly in order to split up a complex decision tree into more manageable
        chunks.
      </p>
      
      <br>
      <p>
        To change your priority set, use the <strong>Current Set</strong> dropdown at the bottom of
        the Arvindroid menu. Click on <button class="avd-cline__btn avd-cline__btn--success">${SPARKLES_MARK}</button>
        to create a new priority set; the new set will immediately become your current set. Click on
        <button class="avd-cline__btn avd-cline__btn--danger">${CROSS_MARK}</button> to delete the current
        priority set.
      </p>
      
      <br>
      <p>
        You can also choose a single priority set to be your <strong>Primary Set</strong>. Whenever you
        encounter a new enemy, your <strong>Current Set</strong> will automatically be switched to your
        <strong>Primary Set</strong>. The name of your <strong>Primary Set</strong>, if any, is displayed above
        the priority list. Click on <button class="avd-cline__btn avd-cline__btn--warning">${STAR_MARK}</button>
        to change your <strong>Primary Set</strong> to match your <strong>Current Set</strong>. Click on
        <button class="avd-cline__btn avd-cline__btn--danger">${CROSS_MARK}</button> to deselect your
        <strong>Primary Set</strong>.
      </p>
    `);
    addHelpPage('Conditions', `
      <p>
        <strong>If Possible</strong>
        <br>
        The default condition type. Takes no argument and is always met.
      </p>
      
      <br>
      <p>
        <strong>Probability</strong>
        <br>
        A condition which is randomly met or not. The argument is an integer representing the percent
        chance that the condition will be met.
      </p>
      
      <br>
      <p>
        <strong>Player HP/MP Below</strong>
        <br>
        Conditions based on the player's current HP/MP. The argument is an integer representing the
        threshold below which the condition will be met.
      </p>
      
      <br>
      <p>
        <strong>Player HP/MP Missing</strong>
        <br>
        Conditions based on the difference between the player's maximum HP/MP and the player's current HP/MP.
        The argument is an integer representing the threshold above which the condition will be met.
      </p>
      
      <br>
      <p>
        <strong>Enemy Name</strong>
        <br>
        A condition based on the name of the current enemy. The condition is met if the argument exactly matches the enemy's name.
      </p>
      
      <br>
      <p>
        <strong>Enemy HP Below/Above</strong>
        <br>
        Conditions based on the enemy's current HP. The argument is an integer representing the threshold
        below/above which the condition will be met.
      </p>
      
      <br>
      <p>
        <strong>Status Active/Inactive</strong>
        <br>
        Conditions based on the currently active status effects. The <strong>Status Active</strong> condition is met if the argument
        exactly matches the "alt" attribute of an icon in the "Battlefield Effects" panel; the <strong>Status Inactive</strong>
        condition is met otherwise.
      </p>
      
      <br>
      <p>
        <strong>Current Zone</strong>
        <br>
        This condition is met if the argument matches the ID of the current combat zone.
      </p>
      
      <br>
      <p>
        <strong>}</strong>
        <br>
        A special condition which is only meaningful when used together with the <strong>{</strong> action.
        Denotes the end of the block controlled by the matching <strong>{</strong> action.
      </p>
      
      <br>
      <p>
        <strong>COMBO TIME</strong>
        <br>
        A special condition which is only met when used together with the <strong>Combo</strong> action.
        When a <strong>Combo</strong> action is performed, Arvindroid begins using the actions associated
        with the following priorities one after the other until one of those actions fails, one of those
        conditions is not <strong>COMBO TIME</strong>, or a new enemy is encountered; once the combo ends,
        Arvindroid resumes normal operation.
      </p>
    `);
    addHelpPage('Actions', `
      <p>
        <strong>Item/Skill</strong>
        <br>
        Use an equipped combat item or skill. The argument is an integer from 1 to 5 representing
        the position of the item/skill on the item/skill bar.
      </p>
      
      <br>
      <p>
        <strong>Flee</strong>
        <br>
        Flee from the current enemy. If auto-restart is active, immediately start a new combat in the current zone.
      </p>
      
      <br>
      <p>
        <strong>Zone</strong>
        <br>
        Flee from the current enemy and immediately start a new combat in another zone. The argument is the ID of
        the new zone.
      </p>
      
      <br>
      <p>
        <strong>AND</strong>
        <br>
        A special action used to apply multiple conditions to a single action. If the condition fails or is ignored,
        then the following priority is ignored.
      </p>
      
      <br>
      <p>
        <strong>OR</strong>
        <br>
        A special action used to set alternative conditions to a single action. If the condition is met, then the first
        following priority that does not have an <strong>AND</strong> or <strong>OR</strong> action is executed.
      </p>
      
      <br>
      <p>
        <strong>{</strong>
        <br>
        A special action used to apply a condition to a group of priorities. If the condition fails or is ignored, then
        all the following priorities are ignored down to the matching <strong>}</strong> condition.
      </p>
      
      <br>
      <p>
        <strong>Combo</strong>
        <br>
        A special action used to unconditionally execute multiple actions in series.
        When a <strong>Combo</strong> action is performed, Arvindroid begins using the actions associated
        with the following priorities one after the other until one of those actions fails, one of those
        conditions is not <strong>COMBO TIME</strong>, or a new enemy is encountered; once the combo ends,
        Arvindroid resumes normal operation.
      </p>
      
      <br>
      <p>
        <strong>Load</strong>
        <br>
        Change the current priority set and use the new priority set to choose an action. The argument is the
        name of the priority set to switch to. This action will fail if the target priority set has already
        been loaded by a <strong>Load</strong> action during the current combat round.
      </p>
    `);
    addHelpPage('Action Usability', `
      <p>
        In order to prevent getting stuck in a horrific whirlwind of fail, Arvindroid remembers any time
        that it tries to use a combat item or skill and fails. When the selected item or skill fails due to
        insufficient ammo/MP/whatever, that item/skill slot will be marked as unusable. This is represented
        by the ten <span style='color: ${CSS_COLORS.green.light}'>${SQUARE_MARK}</span> on the navbar link;
        if, for example, skill slot 4 becomes unusable, the 4th <span style='color: ${CSS_COLORS.green.light}'>${SQUARE_MARK}</span>
        on bottom row in the navbar link will turn red. Once a slot has become unusable, the only way to reset it
        is manually via the <button class="avd-cline__btn avd-cline__btn--info">Reset Action Usability</button> button.
      </p>
    `);
    
    droidUl.append(droidHelp);

    let whatDoBadge = document.createElement('span');
    whatDoBadge.id = 'arvindroid-what-do';
    whatDoBadge.className = 'badge';
    whatDoBadge.textContent = '?';
    whatDoBadge.addEventListener('click', function() {
      droidHelp.classList.toggle('avd-help--open');
    });
    droidUl.append(whatDoBadge);
    
    
    let updateStatusButton = function(button, isGreen) {
      if (button.classList.contains('avd-cline__btn--success') != isGreen) {
        button.classList.toggle('avd-cline__btn--danger');
        button.classList.toggle('avd-cline__btn--success');
        statusBadge.innerHTML = status.settings.active ? status.settings.autoRestart ? STAR_MARK : CHECK_MARK : CROSS_MARK;
      }
    };
    
    let statusButtonsLine = document.createElement('li');
    statusButtonsLine.className = 'avd-cline';
    statusButtonsLine.innerHTML = `
      <button class="avd-cline__btn avd-cline__btn--danger">Toggle Status</button>
      <button class="avd-cline__btn avd-cline__btn--danger">Toggle Auto Restart</button>
      ${FILLER_SPAN_HTML}
      <button class="avd-cline__btn avd-cline__btn--info">Reset Action Usability</button>
    `;
    
    let activeButton = statusButtonsLine.children[0];
    let autoRestartButton = statusButtonsLine.children[1];
    let resetActionUsabilityButton = statusButtonsLine.children[3];
    
    activeButton.addEventListener('click', function() {
      status.settings.active = !status.settings.active;
      status.storageScheduler.set();
      updateStatusButton(this, status.settings.active);
    });
    autoRestartButton.addEventListener('click', function() {
      status.settings.autoRestart = !status.settings.autoRestart;
      status.storageScheduler.set();
      updateStatusButton(this, status.settings.autoRestart);
    });
    resetActionUsabilityButton.addEventListener('click', function() {
      status.resetActionUsability();
      ui.update();
    });
    
    droidUl.appendChild(statusButtonsLine);
    
    
    let primarySetLine = document.createElement('li');
    primarySetLine.className = 'avd-cline';
    primarySetLine.innerHTML = `
      <strong>Primary Set:</strong>
      <span class="badge flex-fill">${status.primarySet}</span>
      <button class="avd-cline__btn avd-cline__btn--warning">${STAR_MARK}</button>
      <button class="avd-cline__btn avd-cline__btn--danger">${CROSS_MARK}</button>
    `;
    
    let primarySetLabel = primarySetLine.children[0];
    let primarySetBadge = primarySetLine.children[1];
    let changePrimarySetButton = primarySetLine.children[2];
    let clearPrimarySetButton = primarySetLine.children[3];
    
    changePrimarySetButton.addEventListener('click', function() {
      status.settings.primarySet = status.settings.currentSet;
      primarySetBadge.textContent = status.settings.primarySet;
      status.storageScheduler.set();
    });
    clearPrimarySetButton.addEventListener('click', function() {
      status.settings.primarySet = '';
      primarySetBadge.textContent = '';
      status.storageScheduler.set();
    });
    
    droidUl.appendChild(primarySetLine);
    
    
    let colorActionDots = function() {
      (['Item', 'Skill']).forEach(function(bar, barIndex) {
        (['1', '2', '3', '4', '5']).forEach(function(action, actionIndex) {
          actionDots[barIndex][actionIndex].style.color = status.settings.actionUsability[bar][action] ? CSS_COLORS.green.light : CSS_COLORS.red.light;
        });
      });
    };
    
    ui.sync.status = function() {
      colorActionDots();
      updateStatusButton(activeButton, status.settings.active);
      updateStatusButton(autoRestartButton, status.settings.autoRestart);
      dropdown.setAttribute('theme', status.settings.theme);
      primarySetBadge.textContent = status.settings.primarySet;
    };
  }
  
  
  /*
   * Priority UI injection
   */
   
   
  {
    const NUM_PLINES = 20;
  
    droidUl.addEventListener('change', function() {
      priorities.storageScheduler.set(status.settings.currentSet);
    });
    
    let prioritySubmenu = document.createElement('li');
    prioritySubmenu.className = 'submenu';
    prioritySubmenu.innerHTML = `
      <ul class="nav avd-plist">
        <div class="avd-plist__scroll">
          <div></div>
        </div>
        <div class="avd-plist__viewport">
          <div class="avd-plist__display">
            <div class="avd-plist__filler"></div>
            <div class="avd-plist__filler"></div>
          </div>
        </div>
      </ul>
    `;
    
    let priorityList = prioritySubmenu.children[0];
    let priorityListScroll = priorityList.children[0];
    let priorityListViewport = priorityList.children[1];
    let priorityListDisplay = priorityListViewport.children[0];
    
    droidUl.appendChild(prioritySubmenu);
    
    const PRIORITY_LINE_NODE = document.createElement('li');
    PRIORITY_LINE_NODE.innerHTML = `
      <span class="badge">${SPARKLES_MARK}</span>
      <span class="avd-plist__highlight"></span>
      ${conditionSelectString}
      <input type="text" class="avd-hidden"></input>
      ${actionSelectString}
      <input type="text"></input>
      <span class="avd-plist__highlight"></span>
      <span class="badge">${CROSS_MARK}</span>
    `;
    PRIORITY_LINE_NODE.className = 'text-center avd-pline';
    
    priorityListDisplay['avd-priority-index'] = 0;
    priorityListDisplay['avd-target-scroll'] = 0;
    
    for (let i = 0; i < NUM_PLINES; i++) {
      let lineNode = PRIORITY_LINE_NODE.cloneNode(true);
      
      lineNode['avd-priority-offset'] = i;
      
      lineNode.addEventListener('change', function() {
        let index = this.parentNode['avd-priority-index'] + this['avd-priority-offset'];
        let priority = priorities.model.data[index];
        priority.condition.type = this.children[2].value;
        priority.condition.arg = this.children[3].value;
        priority.action.type = this.children[4].value;
        priority.action.arg = this.children[5].value;
        ui.clearHighlights();
        ui.update();
      });
      lineNode.children[0].addEventListener('click', function() {
        priorities.add(this.parentNode.parentNode['avd-priority-index'] + this.parentNode['avd-priority-offset']);
        ui.update();
      });
      lineNode.children[7].addEventListener('click', function() {
        priorities.remove(this.parentNode.parentNode['avd-priority-index'] + this.parentNode['avd-priority-offset']);
        ui.update();
      });
      
      priorityListDisplay.insertBefore(lineNode, priorityListDisplay.lastElementChild);
    }
    
    let syncScrollHeight = function() {
      priorityListScroll.children[0].style.height = `${priorities.model.data.length * 24}px`;
    };
    
    let scrollTarget = 240;
    var updatePriorityLines = function() {
      const UiLine = {
        conditionType: function(lineNode) { return lineNode.children[2]; },
        conditionArg: function(lineNode) { return lineNode.children[3]; },
        conditionSpan: function(lineNode) { return lineNode.children[1]; },
        actionType: function(lineNode) { return lineNode.children[4]; },
        actionArg: function(lineNode) { return lineNode.children[5]; },
        actionSpan: function(lineNode) { return lineNode.children[6]; },
      };
      
      let myPriorities = priorities.model.data;
      
      let offset = Math.floor(priorityListDisplay['avd-target-scroll'] / 24) - 5;
      if (offset < 0) {
        offset = 0;
      }
      else if (offset % 2 === 1) { // for consistent striping
        offset--;
      }
      
      priorityListDisplay['avd-priority-index'] = offset;
      
      let lineNodes = priorityListDisplay.children;
      let myCONDITIONS = CONDITIONS;
      let myACTIONS = ACTIONS;
      for (let i = 0; i < NUM_PLINES; i++) {
        let priority = myPriorities[offset + i];
        let lineNode = lineNodes[1 + i];
        
        if (priority === undefined) {
          if (lineNode.classList.contains('avd-pline--hidden')) {
            break;
          }
          lineNode.classList.add('avd-pline--hidden');
        }
        else {
          lineNode.classList.remove('avd-pline--hidden');
          
          UiLine.conditionType(lineNode).value = priority.condition.type;
          UiLine.conditionArg(lineNode).value = priority.condition.arg;
          UiLine.actionType(lineNode).value = priority.action.type;
          UiLine.actionArg(lineNode).value = priority.action.arg;
          
          let condition = myCONDITIONS[priority.condition.type];
          let action = myACTIONS[priority.action.type];
          
          UiLine.conditionArg(lineNode).classList.toggle('avd-hidden', !condition.showArg);
          UiLine.actionType(lineNode).classList.toggle('avd-hidden', !condition.showAction);
          UiLine.actionArg(lineNode).classList.toggle('avd-hidden', !condition.showAction || !action.showArg);
          
          UiLine.conditionArg(lineNode).placeholder = condition.placeholder;
          UiLine.actionArg(lineNode).placeholder = action.placeholder;
          
          UiLine.conditionSpan(lineNode).setAttribute('highlight', ui.conditionHighlights[offset + i]);
          UiLine.actionSpan(lineNode).setAttribute('highlight', ui.actionHighlights[offset + i]);
        }
      }
      scrollTarget = 240 + priorityListDisplay['avd-target-scroll'] - priorityListDisplay['avd-priority-index'] * 24;
      if (scrollTarget > 720) {
        scrollTarget = 720;
      }
      else if (scrollTarget < 240) {
        scrollTarget = 240;
      }
      
      priorityListDisplay.scrollTop = scrollTarget;
    };
    
    {
      let cooldownTask = {
        task: null,
        ready: true,
        pending: false,
        cooldown: 100,
        
        boundReset: function() {
          let myBoundReset = this.reset.bind(this);
          this.boundReset = function() {
            return myBoundReset;
          };
          return myBoundReset;
        },
        run: function() {
          this.ready = false;
          this.pending = false;
          this.task();
          setTimeout(this.boundReset(), this.cooldown);
        },
        reset: function () {
          this.ready = true;
          if (this.pending) {
            this.run();
          }
        },
        request: function() {
          if (this.ready) {
            this.run();
          }
          else {
            this.pending = true;
          }
        },
        
        create: function(task) {
          let res = Object.create(cooldownTask);
          res.task = task;
          return res;
        }
      };
      let scrollTask = cooldownTask.create(updatePriorityLines);
      
      let manualScroll = false;
      priorityListScroll.addEventListener('scroll', function(e) {
        priorityListDisplay['avd-target-scroll'] = priorityListScroll.scrollTop;
        priorityListDisplay.scrollTop = scrollTarget = 240 + priorityListScroll.scrollTop - priorityListDisplay['avd-priority-index'] * 24;
        scrollTask.request();
      });
      priorityListDisplay.addEventListener('scroll', function(e) {
        this.scrollTop = scrollTarget;
      });
      priorityListDisplay.addEventListener('wheel', function(e) {
        priorityListScroll.scrollTop += e.deltaMode ? e.deltaY * 14 : e.deltaY;
        e.preventDefault();
      }, true);
    }
    
    let priorityListControlLine = document.createElement('li');
    priorityListControlLine.className = 'avd-cline';
    priorityListControlLine.innerHTML = `
      <button class="avd-cline__btn avd-cline__btn--success">${SPARKLES_MARK} New Priority</button>
      ${FILLER_SPAN_HTML}
      <button class="avd-cline__btn avd-cline__btn--danger">Clear Priorities ${CROSS_MARK}</button>
    `;
    
    let newPriorityButton = priorityListControlLine.children[0];
    let clearPrioritiesButton = priorityListControlLine.children[2];
    
    newPriorityButton.addEventListener('click', function() { 
      priorities.add();
      ui.update();
    });
    clearPrioritiesButton.addEventListener('click', function() {
      if (!window.confirm('Remove all priorities from the current set?')) {
        return;
      }
      priorities.clear();
      ui.update();
    });
    
    droidUl.appendChild(priorityListControlLine);
    
    
    let currentSetLine = document.createElement('li');
    currentSetLine.className = 'avd-cline';
    currentSetLine.innerHTML = `
      <strong>Current Set: </strong>
      <select class="flex-fill"></select>
      <button class="avd-cline__btn avd-cline__btn--success">${SPARKLES_MARK}</button>
      <button class="avd-cline__btn avd-cline__btn--danger">${CROSS_MARK}</button>
    `;
    
    let setNameSelect = currentSetLine.children[1];
    let newSetButton = currentSetLine.children[2];
    let deleteSetButton = currentSetLine.children[3];
    
    let syncSetNames = function() {
      let newHTML = [];
      Object.keys(localStorage).filter(function(key) { return key.startsWith(priorities.STORAGE_KEY); }).forEach(function(key) {
        newHTML.push(`<option>${key.substr(priorities.STORAGE_KEY.length + 1)}</option>`);
      });
      setNameSelect.innerHTML = newHTML.join('');
      setNameSelect.value = status.settings.currentSet;
    };
    
    setNameSelect.addEventListener('change', function() {
      priorities.switch(this.value);
      ui.update();
    });
    newSetButton.addEventListener('click', function() {
      let newSet = window.prompt("Name your new priority set.");
      if (newSet !== null) {
        priorities.storageScheduler.resolve();
        status.settings.currentSet = newSet;
        status.storageScheduler.set();
        priorities.clear();
        priorities.store(newSet);
        ui.update();
      }
    });
    deleteSetButton.addEventListener('click', function() {
      if (!window.confirm('Permanently delete the current priority set?')) {
        return;
      }
      var toDelete = status.settings.currentSet;
      priorities.switch('');
      priorities.delete(toDelete);
      ui.update();
    });
    
    droidUl.appendChild(currentSetLine);
    

    ui.sync.priorities = function() {
      syncSetNames();
      syncScrollHeight();
      updatePriorityLines();
    };
  }
  
  
  /*
   * Hijack combat functions
   */
   
   
  {
    // battle function
    $(document).ajaxSuccess(function(e, data, xhr, text) {
      if (/adventure\/(start|continue)/.test(xhr.url)) {
        afterCombatRequest();
      }
    });
    
    let afterCombatRequest = function() {
      let combatPanel = document.querySelector('div#combat_details div.combat-panel');
      if (!combatPanel) {
        console.log('eh...');
        return;
      }
      
      let combatLog = combatPanel.querySelector('div:last-of-type');
      if (combatLog && combatLog.textContent.trim() === '') {
        return; // the previous combat request failed; wait for a successful one before choosing an action
      }
      
      combat.meta.lastCombatLog = combatLog;
      
      combat.updateState();
      combat.decideNextAction();
      ui.update();
    };
    
    let oldBattle = battle;
    battle = function(e) {
      dropdown.classList.add('dim');
      if (status.settings.active && (window.combat_action === undefined || window.combat_action === '') && combat.meta.priorityAction !== null) {
        combat.meta.usedPriorityAction = true;
        switch(combat.meta.priorityAction.type) {
          case 'Item':
          case 'Skill':
            combat.meta.priorityActionButton.click();
            break;
          
          case 'Flee':
            if (status.settings.autoRestart) {
              startAdventure(status.settings.combatZone);
            }
            else {
              stopAdventure();
            }
            return;
            
          case 'Zone':
            startAdventure(status.settings.combatZone);
            return;
          
          default:
            break;
        }
      }
      oldBattle(e);
    };
    
    // stop combat function
    let oldStopAdventure = stopAdventure;
    stopAdventure = function() {
      dropdown.classList.add('dim');
      oldStopAdventure();
    };

    // start combat function
    let oldStartAdventure = startAdventure;
    startAdventure = function(zoneId) {
      if (zoneId !== status.combatZone) {
        status.settings.combatZone = zoneId;
        status.storageScheduler.set();
      }
      combat.meta.comboTarget = -1;
      if (status.settings.primarySet !== '' && status.settings.primarySet !== status.settings.currentSet) {
        priorities.switch(status.settings.primarySet);
        ui.update();
      }
      
      oldStartAdventure(zoneId);
    };
  }

   
  /*
   * Pageload UI initialization
   */
   
   
  {
    status.load();
    priorities.switch(status.settings.currentSet);
    
    if (status.settings.active && status.settings.autoRestart && (status.settings.combatZone !== -1) && (location.pathname.startsWith('/combat_zones'))) {
      let targetZone = status.settings.combatZone;
      let tryStartCombat = function() {
        if (! (status.settings.active && status.settings.autoRestart)) {
          return true;
        }
        else if (document.querySelector(`button[onclick^="startAdventure(${targetZone});"]`)) {
          startAdventure(targetZone);
          return true;
        }
        return false;
      };
      if (! tryStartCombat()) {
        $('<a href="/combat_zones/' + targetZone + '/adventure" hidden=""></a>').appendTo('body')[0].click();
        let autoStartInterval = setInterval(function() {
          if (tryStartCombat()) {
            clearInterval(autoStartInterval);
          }
        }, 1000);
      }
    }
    
    let attachDropdown = function() {
      let avdParent = document.querySelector('ul.nav.navbar-nav.navbar-right');
      if (!avdParent) {
        setTimeout(attachDropdown, 1000);
        return;
      }
      
      let avdNode = avdParent.querySelector('#arvindroid');
      if(avdNode !== dropdown) {
        if (avdNode) {
          avdParent.removeChild(avdNode);
        }
        avdParent.insertBefore(dropdown, avdParent.firstChild);
        ui.sync.priorities();
      }
    };

    attachDropdown();
    document.addEventListener('turbolinks:load', attachDropdown);
    
    ui.update();
  }
  
  
})();

